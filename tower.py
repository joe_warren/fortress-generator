#!/usr/bin/env python3
from csg.core import CSG
import extra_ops
import math
eps = 0.001
detail = 64
import wall

def spokes(count, radius, length, height):
    gap=CSG.cube(radius=[radius/2, length/2, height/2])
    gap.translate(disp=[radius/2, 0, 0])
    return extra_ops.spokify(gap, count)

def basic_cylindrical_parapet(radius, width, height, crenellations, crenellation_ratio, crenellation_height):
    basis = CSG.cylinder(radius=radius, start=[0,0,0], end=[0,0,height], slices=detail)
    hole =  CSG.cylinder(radius=radius-width, start=[0,0,-eps], end=[0,0,height+eps], slices=detail)
    gap_length = 2.0*math.pi*radius*crenellation_ratio/crenellations
    s = spokes(crenellations, radius, gap_length, crenellation_height+2*eps)
    s.translate(disp=[0,0,height-crenellation_height/2+eps])
    hole = hole.union(s)
    return basis.subtract(hole)

def overhanging_cylindrical_parapet(radius, width, height, crenellations, crenellation_ratio, crenellation_height, overhang_angle):
    basis = basic_cylindrical_parapet(radius + width, width, height+width, crenellations, crenellation_ratio, crenellation_height)
    basis.translate(disp=[0,0,-width])
    if overhang_angle == 0:
       return basis
    mask_ratio = math.tan(math.radians(90-overhang_angle))
    mask_point = -width-radius/mask_ratio
    mask_height = height - mask_point
    mask_radius = mask_height * mask_ratio
    mask = CSG.cone(end=[0,0,mask_point], start=[0,0,height], radius=mask_radius, slices=detail) 
    return basis.intersect(mask)


def crenellated_cylindrical_roof(radius, width, parapet_height, crenellations, crenellation_ratio, crenellation_height, overhang_angle, roof_radius, roof_height):
    parapet = overhanging_cylindrical_parapet(radius, width, parapet_height, crenellations, crenellation_ratio, crenellation_height, overhang_angle)
    roof = CSG.cone(end=[0,0,roof_height], start=[0,0,0], radius=roof_radius, slices=detail) 
    return parapet.union(roof)

def cylindrical_roof(radius, overhang, height):
    bottom = CSG.cone(end=[0,0,height/2], start=[0,0,0], radius=radius+overhang, slices=detail) 
    top = CSG.cone(end=[0,0,height], start=[0,0,0], radius=radius, slices=detail) 
    return bottom.union(top)

def cylindrical_tower(radius, height, roof, roof_info):
    basis = CSG.cylinder(radius=radius, start=[0,0,0], end=[0,0,height], slices=detail)
    roof_info["radius"]=radius

    roof_options = {
      "basic":basic_cylindrical_parapet,
      "overhanging":overhanging_cylindrical_parapet,
      "crenellated_roof":crenellated_cylindrical_roof,
      "roof": cylindrical_roof
    }
    p = roof_options[roof](**roof_info)
    p.translate(disp=[0,0,height])
    return basis.union(p)

def basic_polygonal_parapet(radius, width, height, sides, crenellations, crenellation_ratio, crenellation_height):
    per_wall_crenellations = 2 + math.floor(crenellations/sides)
    length = radius*2*math.tan(math.pi/sides)
    strip = wall.crenellated_strip(length, width, height, per_wall_crenellations, crenellation_ratio, crenellation_height)
    strip.translate(disp=[-length/2, radius-width, 0])
    parapet = extra_ops.spokify(strip, sides)

    bounds = extra_ops.prism(radius, height, sides)
    return bounds.intersect(parapet)



def overhanging_polygonal_parapet(sides, radius, width, height, overhang_angle, **kwargs):

    parapet = basic_polygonal_parapet(radius+width, width, height, sides, **kwargs)
    length = radius*2*math.tan(math.pi/sides)
    mask = CSG.cube(radius=[length, width*2, width*2])
    mask.translate(disp=[0, -width*2,-width*2])
    mask.rotate(axis=[1,0,0], angleDeg=overhang_angle)
    mask.rotate(axis=[0,0,1], angleDeg=180)
    mask.translate(disp=[0, radius, 0])
    mask = extra_ops.spokify(mask, sides)
   
    parapet = parapet.subtract(mask)
    parapet.translate(disp=[0,0,-width])
    return parapet

def crenellated_polygonal_roof(sides, radius, width, parapet_height, roof_radius, roof_height, **kwargs):
    parapet = overhanging_polygonal_parapet(sides, radius, width, parapet_height, **kwargs)
    roof = extra_ops.pyramid(roof_radius, roof_height, sides)
    return parapet.union(roof)

def polygonal_roof(sides, radius, overhang, height):
    roof = extra_ops.pyramid(radius+overhang, height/2, sides)
    return roof.union(extra_ops.pyramid(radius, height, sides))

def polygonal_tower(radius, height, sides, roof, roof_info):
    basis = extra_ops.prism(radius, height, sides)
    roof_info["sides"]=sides
    roof_info["radius"]=radius

    roof_options = {
      "basic":basic_polygonal_parapet,
      "overhanging":overhanging_polygonal_parapet,
      "crenellated_roof":crenellated_polygonal_roof,
      "roof": polygonal_roof
    }
    r = roof_options[roof](**roof_info)
    r.translate(disp=[0,0,height])
    t =  basis.union(r)
    t.rotate(axis=[0,0,1], angleDeg=180)
    return t

def tower(side_type, **kwargs):
    type_options= {
      "cylindrical": cylindrical_tower,
      "polygonal": polygonal_tower
    }
    return type_options[side_type](**kwargs)
