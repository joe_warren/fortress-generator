#!/usr/bin/env python3
import wall
import tower
import math
def extra_wall_length(width, radius):
    subradius = math.sqrt(radius**2 - width**2/4)
    return radius - subradius


def perimeter(radius, sides, wall_info, tower_info, door_info):
    exterior_angle = 2.0*math.pi/sides
    side_length = 2.0*radius*math.sin(exterior_angle/2.0)
    ewl = extra_wall_length(wall_info["width"], tower_info["radius"])
    wall_info["length"] = side_length - 2*tower_info["radius"] + 2*ewl

    if(tower_info["side_type"]=="polygonal"):
        tower_info["sides"] = 4 if sides == 4 else 3

    t = tower.tower(**tower_info)
    if(sides !=4):
        t.rotate(axis=[0,0,1], angleDeg=math.degrees(exterior_angle)/2)

    w = wall.wall(**wall_info)

    wall.add_wall_info_to_door_info(wall_info, door_info)
    door_wall = wall.with_door(w, **door_info)

    def build_side(t, w):
        w.translate(disp=[tower_info["radius"]-ewl, -wall_info["width"]/2,0.0])
        side = t.union(w)
        #side.rotate(axis=[0,0,1], angleDeg=180-math.degrees(exterior_angle/2.0))
        side.translate(disp=[-side_length/2, -radius*math.cos(exterior_angle/2.0), 0])
        return side

    obj = build_side(t, door_wall)
    side = build_side(t, w)
   
    for i in range(1, sides):
        s = side.clone()
        s.rotate(axis=[0,0,1], angleDeg= math.degrees(-i*exterior_angle))
        obj = obj.union(s)
    return obj
