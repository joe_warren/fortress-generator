#!/usr/bin/env python3
from csg.core import CSG
import megaplex
import perimeter
import wall
import tower
import description
import json
from obj_writer import print_obj

megaplex_info = description.megaplex()
#print(json.dumps(megaplex_info, indent=2))
shape = megaplex.megaplex(**megaplex_info)
polygons = shape.toPolygons()


print_obj(*shape.toVerticesAndPolygons())
