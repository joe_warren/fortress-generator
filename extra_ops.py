#!/usr/bin/env python3
from csg.core import CSG
import csg.geom
import math
from functools import reduce

eps = 0.001


def scale_vertex(v, s):
   return csg.geom.Vertex([a[0]*a[1] for a in zip(v.pos, s)])

def scale_polygon(p, s):
    return csg.geom.Polygon([scale_vertex(v,s) for v in p.vertices])

def scale(obj, s):
    obj.polygons = [scale_polygon(p,s) for p in obj.polygons]

def spokify(entity, count):
    def spoke(i):
        s = entity.clone() 
        s.rotate(axis=[0,0,1], angleDeg= 360 * i/count)
        return s
    def union(a, b):
        return a.union(b)
    return reduce(union, (spoke(i) for i in range(0,count)))

def prism(radius, height, sides):
    basis = CSG.cube(radius=[radius*2, radius*2 ,height/2.0])
    basis.translate(disp=[0,0,height/2.0])
    mask = CSG.cube(radius=[radius*2, radius*2+eps,height/2.0+eps])
    mask.translate(disp=[0, 3*radius, height/2])
    mask = spokify(mask, sides)
    return basis.subtract(mask)

def pyramid(radius, height, sides):
    basis= CSG.cube(radius=[radius*2,radius*2,height/2])
    basis.translate(disp=[0,0,height/2])
    hypo = math.sqrt(radius**2 + height**2)
    mask=CSG.cube(radius=[radius*4,radius*4, hypo/2+eps])
    mask.translate(disp=[0,radius*4,hypo/2])
    angled = mask.clone()
    angled.rotate(axis=[1,0,0], angleDeg=-math.degrees(math.acos(height/hypo)))
    mask = mask.union(angled)
    mask.translate(disp=[0,radius,0])
    mask = spokify(mask, sides)
    return basis.subtract(mask)

