#!/usr/bin/env python3
from csg.core import CSG
from extra_ops import scale
import math
eps = 0.001

def crenellated_strip(length, width, height, crenellations, crenellation_ratio, crenellation_height):
   basis = CSG.cube(radius=[length/2, width/2, height/2])
   basis.translate(disp=[length/2.0, width/2.0, height/2.0])
   gap_count = crenellations -1
   gap_length = float(length * crenellation_ratio)/gap_count
   for i in range(1,gap_count+1):
      gap = CSG.cube(radius=[gap_length/2, width/2+eps, crenellation_height/2+eps])
      gap.translate(disp=[0, width/2-eps/2.0, height + eps - crenellation_height/2])
      gap.translate(disp=[i*float(length)/crenellations, 0.0,0.0])
      basis = basis.subtract(gap)
   return basis


def basic_parapet(length, width, height, crenellations, crenellation_ratio, crenellation_height, two_sided, parent_width):
   basis = crenellated_strip( length, width, height, crenellations, crenellation_ratio, crenellation_height)
   if two_sided:
       back=basis.clone()
       back.translate(disp=[0,parent_width-width,0])
       basis=basis.union(back)
   return basis

def cut_overhang_angle_off_strip(strip, length, width, overhang_angle):
   strip.translate(disp=[0,-width, -width])
   mask = CSG.cube(radius=[length/2, width*2, width*2])
   mask.translate(disp=[length/2, -width*2,-width*2])
   mask.rotate(axis=[1,0,0], angleDeg=overhang_angle)
   mask.translate(disp=[0,0, -width])
   strip = strip.subtract(mask)

def overhanging_parapet(length, width, height, crenellations, crenellation_ratio, crenellation_height, overhang_angle, two_sided, parent_width):
   basis = crenellated_strip(length, width, height+width, crenellations, crenellation_ratio, crenellation_height)
   cut_overhang_angle_off_strip(basis, length, width, overhang_angle)
   if two_sided:
       back=basis.clone()
       back.rotate(axis=[0,0,1], angleDeg = 180)
       back.translate(disp=[length,parent_width,0])
       basis=basis.union(back)
   return basis

parapet_options = {
  "basic":basic_parapet,
  "overhanging":overhanging_parapet
}

def wall(length, width, height, parapet, parapet_info):
   basis = CSG.cube(radius=[length/2, width/2, height/2])
   basis.translate(disp=[length/2.0, width/2.0, height/2.0])
   parapet_info["length"]=length
   parapet_info["parent_width"] = width
   front = parapet_options[parapet](**parapet_info)
   front.translate(disp=[0.0, 0.0, height])
   return basis.union(front)

def add_wall_info_to_door_info(wall_info, door_info):
    door_info["parent_length"] = wall_info["length"]
    door_info["parent_width"] = wall_info["width"]

detail = 64
def arch(width, depth, height):
    h = height
    if height < width: 
        h = width*1.5
    r= h**2/width - width/4
    d = r - width/2
    left = CSG.cylinder(radius=r, start=[0,0,0], end=[0,depth,0], slices=detail)
    right = left.clone()
    left.translate(disp=[d, 0, 0])
    right.translate(disp=[-d, 0, 0])
    mask = CSG.cube(radius=[width*2, depth*2, h])
    mask.translate(disp=[0,0,-h])
    obj = left.intersect(right).subtract(mask)

    if h!=height:
        scale(obj, [1.0,1.0,height/h])
    return obj

def with_door(wall, width, height, parent_width, parent_length, border_width, arched, arch_height): 
    border_height = height+border_width
    if arched:
       border_height = height
    border = CSG.cube(radius=[border_width+width/2, border_width+parent_width/2, border_height/2])
    border.translate(disp=[(parent_length)/2,parent_width/2, border_height/2])
    if arched:
        border_arch = arch(width+2*border_width, parent_width+2*border_width, arch_height+border_width)
        border_arch.translate(disp=[parent_length/2,-border_width,height])
        border = border.union(border_arch)
    door_hole = CSG.cube(radius=[width/2, border_width+parent_width/2+eps, height/2])
    door_hole.translate(disp=[(parent_length)/2,parent_width/2, height/2])
    if arched:
        hole_arch = arch(width, parent_width+2*border_width+eps*2, arch_height)
        hole_arch.translate(disp=[parent_length/2,-border_width-eps,height])
        door_hole = door_hole.union(hole_arch)
    return wall.union(border).subtract(door_hole)
