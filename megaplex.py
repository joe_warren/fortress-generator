#!/usr/bin/env python3
import perimeter
import tower

def megaplex(inner_perimeter_info, outer_perimeter_info, tower_info):
    outer_perimeter = perimeter.perimeter(**outer_perimeter_info)
    inner_perimeter = perimeter.perimeter(**inner_perimeter_info)
    central_tower = tower.tower(**tower_info)
    return outer_perimeter.union(inner_perimeter).union(central_tower)
