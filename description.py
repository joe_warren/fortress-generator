import random

def megaplex():
  return {
      "tower_info": {
        "height": random.uniform(13,25),
        "radius":random.uniform(1.5, 3),
    
        **random.choice([{
          "side_type": "polygonal",
          "sides":random.randrange(3,8)
         }, {
          "side_type": "cylindrical"
         }]),
        **random.choice([{
          "roof":"basic",
          "roof_info":{
            "width": random.uniform(0.05, 0.4),
            "height":random.uniform(0.2, 1),
            "crenellations":random.randrange(5,20),
            "crenellation_ratio":random.uniform(0.2, 0.8),
            "crenellation_height":random.uniform(0.05,0.2)
          }
        }, {
          "roof":"overhanging",
          "roof_info":{
            "width": random.uniform(0.05, 0.4),
            "height":random.uniform(0.2, 1),
            "crenellations":random.randrange(5,20),
            "crenellation_ratio":random.uniform(0.2, 0.8),
            "crenellation_height":random.uniform(0.05,0.2),
            "overhang_angle":random.choice([0,30, 45, 60]),
           }
         }, {
          "roof":"crenellated_roof",
          "roof_info":{
            "width": random.uniform(0.05, 0.4),
            "parapet_height":random.uniform(0.2, 1),
            "crenellations":random.randrange(5,20),
            "crenellation_ratio":random.uniform(0.2, 0.8),
            "crenellation_height":random.uniform(0.05,0.2),
            "overhang_angle":random.choice([0,30, 45, 60]),
            "roof_radius":random.uniform(0.5,2.0),
            "roof_height":random.uniform(0.5, 3.0)
          }
        }, {
          "roof":"roof",
          "roof_info":{
            "overhang": random.uniform(0.1, 0.5),
            "height":random.uniform(1.0, 5.0)
          }
        }])
      },
      "inner_perimeter_info" : {
        "radius":random.uniform(10, 15),
        "sides":random.randrange(3, 6),
        "wall_info":{
          "width":random.uniform(1.5,3),
          "height":random.uniform(5, 8),
           **random.choice([{
            "parapet":"basic",
            "parapet_info":{
              "width": random.uniform(0.1, 0.4),
              "height":random.uniform(0.2,1.0),
              "crenellations":random.randrange(3, 15),
              "crenellation_ratio":random.uniform(0.2, 0.8),
              "crenellation_height":random.uniform(0.05, 0.2),
              "two_sided": random.choice([True, False]),
            }
           },{
            "parapet":"overhanging",
            "parapet_info":{
              "width": random.uniform(0.1, 0.4),
              "height":random.uniform(0.2,1.0),
              "crenellations":random.randrange(3, 15),
              "crenellation_ratio":random.uniform(0.2, 0.8),
              "crenellation_height":random.uniform(0.05, 0.2),
              "two_sided": random.choice([True, False]),
              "overhang_angle":random.choice([0,30, 45, 60])
            }
          }])
        },
        "door_info":{
          "width":random.uniform(1.0, 3.0),
          "height":random.uniform(1.5, 3.0),
          "border_width":random.uniform(0.05, 0.3),
          "arched": random.choice([True, False]),
          "arch_height":random.uniform(0.3, 2.0)
        },
        "tower_info": {
          "height":random.uniform(8,16),
          "radius":random.uniform(1.5, 3),
          "side_type":random.choice(["cylindrical", "polygonal"]),
          **random.choice([{
            "roof":"basic",
            "roof_info":{
              "width": random.uniform(0.05, 0.4),
              "height":random.uniform(0.2, 1),
              "crenellations":random.randrange(5,20),
              "crenellation_ratio":random.uniform(0.2, 0.8),
              "crenellation_height":random.uniform(0.05,0.2)
            }
          }, {
            "roof":"overhanging",
            "roof_info":{
              "width": random.uniform(0.05, 0.4),
              "height":random.uniform(0.2, 1),
              "crenellations":random.randrange(5,20),
              "crenellation_ratio":random.uniform(0.2, 0.8),
              "crenellation_height":random.uniform(0.05,0.2),
              "overhang_angle":random.choice([0,30, 45, 60]),
             }
           }, {
            "roof":"crenellated_roof",
            "roof_info":{
              "width": random.uniform(0.05, 0.4),
              "parapet_height":random.uniform(0.2, 1),
              "crenellations":random.randrange(5,20),
              "crenellation_ratio":random.uniform(0.2, 0.8),
              "crenellation_height":random.uniform(0.05,0.2),
              "overhang_angle":random.choice([0,30, 45, 60]),
              "roof_radius":random.uniform(0.5,2.0),
              "roof_height":random.uniform(0.5, 3.0)
            }
          }, {
          "roof":"roof",
          "roof_info":{
            "overhang": random.uniform(0.1, 0.5),
            "height":random.uniform(1.0, 5.0)
          }
        }])
        }
      }, 
      "outer_perimeter_info" : {
        "radius":random.uniform(20, 25),
        "sides":random.randrange(6, 9),
        "wall_info":{
          "width":random.uniform(1.5,3),
          "height":random.uniform(2.0, 5),
           **random.choice([{
            "parapet":"basic",
            "parapet_info":{
              "width": random.uniform(0.1, 0.4),
              "height":random.uniform(0.2,1.0),
              "crenellations":random.randrange(3, 15),
              "crenellation_ratio":random.uniform(0.2, 0.8),
              "crenellation_height":random.uniform(0.05, 0.2),
              "two_sided": random.choice([True, False]),
            }
           },{
            "parapet":"overhanging",
            "parapet_info":{
              "width": random.uniform(0.1, 0.4),
              "height":random.uniform(0.2,1.0),
              "crenellations":random.randrange(3, 15),
              "crenellation_ratio":random.uniform(0.2, 0.8),
              "crenellation_height":random.uniform(0.05, 0.2),
              "two_sided": random.choice([True, False]),
              "overhang_angle":random.choice([0,30, 45, 60])
            }
          }])
        },
        "door_info":{
          "width":random.uniform(1.0, 3.0),
          "height":random.uniform(1.5, 3.0),
          "border_width":random.uniform(0.05, 0.3),
          "arched": random.choice([True, False]),
          "arch_height":random.uniform(0.3, 2.0)
        },
        "tower_info": {
          "height":random.uniform(4,10),
          "radius":random.uniform(1.5, 3),
          "side_type":random.choice(["cylindrical", "polygonal"]),
          **random.choice([{
            "roof":"basic",
            "roof_info":{
              "width": random.uniform(0.05, 0.4),
              "height":random.uniform(0.2, 1),
              "crenellations":random.randrange(5,20),
              "crenellation_ratio":random.uniform(0.2, 0.8),
              "crenellation_height":random.uniform(0.05,0.2)
            }
          }, {
            "roof":"overhanging",
            "roof_info":{
              "width": random.uniform(0.05, 0.4),
              "height":random.uniform(0.2, 1),
              "crenellations":random.randrange(5,20),
              "crenellation_ratio":random.uniform(0.2, 0.8),
              "crenellation_height":random.uniform(0.05,0.2),
              "overhang_angle":random.choice([0,30, 45, 60]),
             }
           }, {
            "roof":"crenellated_roof",
            "roof_info":{
              "width": random.uniform(0.05, 0.4),
              "parapet_height":random.uniform(0.2, 1),
              "crenellations":random.randrange(5,20),
              "crenellation_ratio":random.uniform(0.2, 0.8),
              "crenellation_height":random.uniform(0.05,0.2),
              "overhang_angle":random.choice([0,30, 45, 60]),
              "roof_radius":random.uniform(0.5,2.0),
              "roof_height":random.uniform(0.5, 3.0)
            }
          }, {
          "roof":"roof",
          "roof_info":{
            "overhang": random.uniform(0.1, 0.5),
            "height":random.uniform(1.0, 5.0)
          }
        }])
        }
      }
    }
