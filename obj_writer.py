#!/usr/bin/env python3
def print_obj(vertices, polygons, count):
    for v in vertices:
        print("v {0} {1} {2}".format(*v))
    for f in polygons:
        print("f " + " ".join([str(i+1) for i in f]))
